# Steam Camp Summer 2021

The idea of this issue tracker and repository is to get the most out of the camp by preparing projects beforehand and by giving the camp an on-line virtual dimension. In practice, this is done by:
 * prior to the camp: the participants (virtual or physical) create a project as an issue Gitlab, ideally relating to the theme of the camp (in this case electromagnetism).
 * during the camp: the participants present their project in a 10-20 minutes session (those who want)
 * after the camp: the participants integrate the feedback received during the camp. Ideally, each project end up as something others could use in the future, either as an actual DIY instructions sheet or simply for inspiration.

A STEAM Camp project in the form of a Gitlab issue is suggested to initially consist of the following elements:
 * a title
 * a short description with the purpose of telling why the project is relevant, what can be learned from it and motivate the reader to continue reading the rest
 * a description of the project's deliverable with the purpose of giving the reader a clear idea of what the expected outcome is

Upon completion, the issue should in addtition to the above include:
 * the list of components and resources needed to complete the project
 * step-by-step instructions

The projects can be in any language, but English is recommended in order to maximize the outreach.
