$fn=64;

Badge= "3D Print";
Size= 8;
difference(){
    cylinder(d=50,h=2.5);
    translate([0,0,2])cylinder(d=48,h=4);
    translate([0,20,-0.5])cylinder(d=5,h=6);
translate([0,-3,1])union(){
translate([0,12,0])linear_extrude(height = 1.5) {
text(str("STEAMCAMP"),font="Liberation Sans:style=Bold", size = 4,halign="center");
    }
    translate([0,5,0])linear_extrude(height = 1.5) {
text(str("2021"),font="Liberation Sans:style=Bold", size = 6,halign="center");
    }
    translate([0,-7,0])linear_extrude(height = 1.5) {
text(str(Badge),font="Liberation Sans:style=Bold",size = Size,halign="center");
    }
}
}
